#!/usr/bin/env python
# -*- coding: utf-8 -*-
n = int(input('Ingresa un entero positivo: '))


X = [] # variable que corresponde a la matriz

for i in range(n):
	
	fila = []
	fila.append(0)

	for j in range(n): #ciclo para generar matriz identidad

		val = 1 if i==j else 0

		fila.append(val)

	X.append(fila) 

print X

