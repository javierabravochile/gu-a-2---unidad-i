#!/usr/bin/env python
# -*- coding: utf-8 -*-

numero = int(input("Ingresa la cantidad de frases de la lista: "))

if numero < 1:
    print("Has ingresado un valor negativo")
else:
    lista = []
    for i in range(numero):
        print("Ingresa la frase, str(i + 1) + ": ", end=")
        palabra = input()
        lista += [palabra]
    print("La lista corresponde a :", lista)
