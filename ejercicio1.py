#!/usr/bin/env python
# -*- coding: utf-8 -*-

palabras = int(input("Ingrese la cantidad de frases que decee: ")) #variable para ingresar cantidad

if palabras < 1: #defino que no puede ser una cantidad negativa
    print("Has ingresado un valor negativo")
else:
    lista = []
    for i in range(palabras): # ciclo el cual recorre cada frase
        print("Ingresa la frase", str(i + 1) + ": ", end="")
        palabra = input()
        lista += [palabra] 
        # variable en dondese le van añadiendo las frases
    print("La lista creada es:", lista)

    inversa = []
    for i in lista: # ciclo donde se recorre cada frase
					# y se genera la frase inversa
        inversa = [i] + inversa
    print("La lista inversa es:", inversa)
